const request = require('request');

module.exports = {
  getExRate:()=>{
  	return new Promise((resolve)=>{
			request("http://api.fixer.io/latest?symbols=HKD,JPY&base=JPY",(err,resp,body)=>{
        body = JSON.parse(body);
				resolve({
					rate:parseInt(body.rates.HKD*1000)
				});
			})
  	})
  }
}
