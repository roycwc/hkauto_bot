var config = require('./config.json');
var TelegramBot = require('node-telegram-bot-api');
var CronJob = require('cron').CronJob;
var storage = require('node-persist');
var Weather = require('./weather');
var Mtr = require('./mtr');
var Nownews = require('./nownews');
var Ex = require('./ex');
var graph = require('fbgraph');
var request = require('request');


function getSummary(){
	return Weather.getWeather().then((temp)=>{
		let text = '宜家'+temp.temp+'度。'
		return text += ' ';
	}).then((text)=>{
		return Ex.getExRate().then((data)=>{
			let rate = data.rate;
			return text + '(¥1000 = $' + rate +')\n\n';
		})
	}).then((text)=>{
		return Mtr.getMtrUpdate().then((update)=>{
			if (update.updated){
				return text + '港鐵'+update.fromNow+'話：\n'+update.text+'\n\n';
			}
			return text;
		})
	}).then((text)=>{
		return Nownews.getNowNewsUpdate(graph).then((news)=>{
			news.map((aNews)=>{
				text += aNews.message+'\n\n'
			})
			return text;
		});
	})
}

function save(chatId){
	return storage.getItem('ids',(savedIds)=>{
		if (!savedIds) savedIds = [];
		if (savedIds.indexOf(chatId) == -1){
			savedIds.push(chatId);
		}
		return storage.setItem('ids', savedIds);
	});
}
function remove(chatId){
	return storage.getItem('ids').then((savedIds)=>{
		if (!savedIds) return;
		let index = savedIds.indexOf(chatId);
		if (index == -1) return;
		savedIds.splice(index, 1);
		return storage.setItem('ids', savedIds);
	});

}

function ini(){
	request('https://graph.facebook.com/oauth/access_token?client_id='+config.fb.key+'&client_secret='+config.fb.secret+'&grant_type=client_credentials', (err, response, body)=>{
		var accessToken = body.replace('access_token=','');
		graph.setAccessToken(accessToken);
		var bot = new TelegramBot(config.tg, { polling: true });
		storage.init({dir:'db'}).then(()=>{
			bot.onText(/\/start/, function (msg) {
			  let chatId = msg.chat.id;
				save(chatId);
				bot.sendMessage(chatId, 'Registered');
			});
			bot.onText(/^all$/i, function (msg) {
				let chatId = msg.chat.id;
				getSummary().then((text)=>{
					bot.sendMessage(chatId, text);
				})
			});
			bot.onText(/^temp$/i, function (msg) {
				let chatId = msg.chat.id;
				Weather.getWeather().then((text)=>{
					bot.sendMessage(chatId, '宜家'+text.temp+'度');
				})
			});
			bot.onText(/^yen/i, function (msg) {
				let chatId = msg.chat.id;
				Ex.getExRate().then((data)=>{
					let text = '¥1000 = $' + data.rate;
					bot.sendMessage(chatId, text);
				})
			});
			bot.onText(/^mtr$/i, function (msg) {
				let chatId = msg.chat.id;
				Mtr.getMtrUpdate().then((update)=>{
					if (update.updated){
						bot.sendMessage(chatId, update.text);
					}else{
						bot.sendMessage(chatId, '服務正常');
					}
				})
			});
			bot.onText(/^news$/i, function (msg) {
				let chatId = msg.chat.id;
				return Nownews.getNowNewsUpdate(graph).then((news)=>{
					var text = '';
					news.map((aNews)=>{
						text += aNews.message+'\n\n'
					})
					return text;
				}).then((text)=>{
					bot.sendMessage(chatId, text);
				})
			});
			bot.onText(/\/unlist/, function (msg) {
			  let chatId = msg.chat.id;
				remove(chatId);
				bot.sendMessage(chatId, 'Unlisted');
			});
			new CronJob('0 0 8 * * *', function() {
				getSummary().then((text)=>{
					storage.getItem('ids').then((savedIds)=>{
						if (!savedIds) return;
						savedIds.map((chatId)=>{
							bot.sendMessage(chatId, text);
						})
					})
				})
			}, null, true,'Asia/Hong_Kong');
		})
	});
}

ini();

process.on('SIGINT', function() {
    process.exit();
});
