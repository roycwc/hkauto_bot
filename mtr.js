var config = require('./config.json');
var Twit = require('twit');
var T = new Twit(config.twit)
var moment = require('moment');

module.exports = {
  getMtrUpdate:()=>{
  	return new Promise((resolve)=>{
  		T.get('statuses/user_timeline.json?count=1&user_id=233829774&screen_name=mtrupdate&lang=zh&trim_user=1', function(err, data, response) {
  			var data = data[0];
        var isMedia = data.entities.hashtags.length > 0;
  			var text = data.text.replace(/ /g,'').replace(/\n\n/g,' ').replace(/\n/g,' ');
        text = text.replace(/https\:\/\/[0-9A-Za-z\/\.]+$/g, '');
  			var createdAt = new Date(data.created_at);
  			var diffHours = (new Date() - createdAt) / 3600000;
  			if (diffHours < 6 && !isMedia){
  				resolve({status:1, updated:1, text:text, createdAt: createdAt, fromNow: moment(createdAt).fromNow().replace(' minutes ago','分前').replace(' hours ago','時前')});
  			}else{
  				resolve({status:1, updated:0});
  			}
  		})
  	})
  }
}
