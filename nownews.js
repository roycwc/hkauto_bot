var graph = require('fbgraph');
var moment = require('moment');

module.exports = {
  getNowNewsUpdate:(graph)=>{
  	return new Promise((resolve)=>{
  		graph.get('now.comNews/posts?limit=5', function(err, res) {
  			let result = res.data.filter((row)=>{
          if (row.message.indexOf('【')===0){
            row.message = row.message.substr(row.message.indexOf('】')+1)
          }
  				let message = row.message.split('【')[0];
  				row.message = message.replace(/http\:\/\/[a-zA-Z\/0-9.]+\n/g,'').replace(/#/g,'').replace(/\n/g,'').replace(/＃/g,'').replace(/ /g, '').trim().split('。')[0]
  				delete row.id;
  				row.fromNow = moment(new Date(row.created_time)).fromNow().replace(' minutes ago','分前').replace(' hours ago','時前')
  				return row;
  			})
  			resolve(result);
  		});
  	})

  }
}
