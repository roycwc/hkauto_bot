var config = require('./config.json');
var Nownews = require('./nownews');
var graph = require('fbgraph');
var request = require('request');
var Mtr = require('./mtr');
var Weather = require('./weather');

function nownews(){
  request('https://graph.facebook.com/oauth/access_token?client_id='+config.fb.key+'&client_secret='+config.fb.secret+'&grant_type=client_credentials', (err, response, body)=>{
    var accessToken = body.replace('access_token=','');
    graph.setAccessToken(accessToken);
    Nownews.getNowNewsUpdate(graph).then((results)=>{
      console.log(results);
    })
  })
}

function mtr(){
  Mtr.getMtrUpdate().then((update)=>{
    console.log(update);
  })
}

function weather(){
  Weather.getWeather().then((temp)=>{
    console.log(temp)
  })
}

mtr();
