const request = require('request');

module.exports = {
  getWeather:()=>{
    return new Promise((resolve)=>{
      request("http://rss.weather.gov.hk/rss/CurrentWeather.xml",(err,resp,body)=>{
        let matches = /Air temperature : ([0-9]+) degrees Celsius\<br\/\>/g.exec(body);
        resolve({
          temp:parseInt(matches[1])
        });
      })
    })
  },
  _getWeather:()=>{
  	return new Promise((resolve)=>{
  		request('http://openweathermap.org/data/2.5/find?callback=jQuery19107111057851722487_1486956697039&q=Hongkong&type=like&sort=population&cnt=30&appid=b1b15e88fa797225412429c1c50c122a1&_=1486956697041', (err, response, body)=>{
  			body = JSON.parse(body.replace('jQuery19107111057851722487_1486956697039(','').slice(0,-1))
  			var temp = body.list[0].main;
  			delete temp.pressure;
  			delete temp.humidity;
  			temp.temp = Math.round(temp.temp - 273.15);
  			temp.temp_min = Math.round(temp.temp_min - 273.15);
  			temp.temp_max = Math.round(temp.temp_max - 273.15);
  			resolve(temp);
  		})
  	})
  }
}
